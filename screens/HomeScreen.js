import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import PrimaryButton from "../components/PrimaryButton";

const GameScreen = props => {
    return (
        <View style={styles.screen}>
            <View style={styles.title}>
                <Text style={styles.titleText}>
                    Welcome to the guessing game. You gotta guess a number. You have 100 points, failing to guess -5
                    points. Hint is -25 points.
                </Text>
            </View>
            <View>
                <PrimaryButton onPress={props.startGame}>
                    Start new game
                </PrimaryButton>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        padding: 10,
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        marginVertical: 10,

    },
    titleText: {
        fontSize: 20,
        marginVertical: 10,
        fontFamily: 'open-sans-bold'
    },
});

export default GameScreen;
