import React from 'react';
import {StyleSheet, Text, View, Button, Image, Dimensions} from 'react-native';
import Colors from "../constants/colors";
import PrimaryButton from "../components/PrimaryButton";

const GameOver = props => {

    return (
        <View style={styles.screen}>
            <View style={styles.title}>
                <Text style={styles.titleText}>
                    {props.message}
                </Text>
            </View>
            <View style={styles.imageWrapper}>
                <Image
                    style={styles.image}
                    // source={require('../assets/images/original.png')}
                    source={{
                        uri: 'https://wrestlingnews.co/wp-content/uploads/2019/11/6F118076-1045-457E-8180-384BCA5DCEF5-scaled-1280x720.png'
                    }}
                    resizeMode='cover'
                    fadeDuration={500}
                />
            </View>
            <PrimaryButton onPress={props.goHome}>
                Go back to home screen
            </PrimaryButton>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        padding: 10,
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    titleText: {
        fontSize: 20,
        marginVertical: 10
    },
    image: {
        width: '100%',
        height: '100%',
    },
    imageWrapper: {
        borderRadius: Dimensions.get('window').width / 3,
        borderWidth: 10,
        borderColor: Colors.primary,
        width: Dimensions.get('window').width / 1.5,
        height: Dimensions.get('window').width / 1.5,
        overflow: 'hidden',
        marginVertical: Dimensions.get('window').height / 20,
    }
});

export default GameOver;
