import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Button, TouchableWithoutFeedback, Keyboard, Alert, FlatList} from 'react-native';
import Card from "../components/Card";
import Colors from "../constants/colors";
import Input from "../components/Input";
import GuessedNumbers from "../components/GuessedNumbers";
import PrimaryButton from "../components/PrimaryButton";
import {Ionicons} from '@expo/vector-icons'

const GameScreen = props => {
    const [enteredValue, setEnteredValue] = useState('');
    const [lastGuessConfirmation, setLastGuessConfirmation] = useState(false);
    const [guessedNumbers, setGuessedNumbers] = useState([]);
    const [points, setPoints] = useState(100);
    const [numberOfGuesses, setNumberOfGuesses] = useState(0);


    const InputHandler = text => {
        setEnteredValue(text.replace(/[^0-9]/g, ''));
    }
    const ResetHandler = () => {
        setEnteredValue('');
    }

    const ConfirmLastGuessHandler = () => {
    }

    const GuessHandler = () => {
        if (enteredValue === '')
            return;

        let val = parseInt(enteredValue)

        if (isNaN(val) || val < 0 || val > 999) {
            Alert.alert(
                'Invalid number',
                'The provided number is required to be within the selected range',
                [
                    {
                        text: 'Alrighty',
                        style: 'destructive',
                        onPress: ResetHandler
                    }
                ]
            )
            return;
        }

        setGuessedNumbers([...guessedNumbers, val]);

        if (val === props.numberToGuess) {
            console.log('yay')
            console.log(guessedNumbers)
            props.endGame(true)
        } else {
            console.log('Wrong')
            setPoints(points - 10)
            if (points === 10) {
                console.log('You lose')
                props.endGame(false)
            }
        }
    }

    // useEffect(() => {
    //     let val = parseInt(enteredValue)
    //
    //     if (val && val === props.numberToGuess) {
    //         props.endGame(true)
    //     }
    // })

    return (
        <TouchableWithoutFeedback onPress={() => {
            Keyboard.dismiss();
        }}>
            <View visible={props.active} style={styles.screen}>
                <View style={styles.title}>
                    <Text style={styles.titleText}>
                        Guessing game
                    </Text>
                </View>
                <Card style={styles.inputContainer}>
                    <Text>Guess a number</Text>
                    <View style={styles.inputWrapper}>
                        <Input style={styles.input}
                               blurOnSubmit autoCapitalize='none' autoCorrect={false}
                               keyboardType='number-pad' maxLength={3}
                               onChangeText={InputHandler}
                               value={enteredValue}
                        />
                    </View>
                    <View>
                        <Text>Points left: {points}</Text>
                    </View>

                    <View style={styles.supportButtons}>
                        <View style={styles.buttonsWrapper}>
                            <PrimaryButton onPress={GuessHandler}>
                                {/*<Ionicons name='ios-heart' size={24}/>*/}
                                Guess
                            </PrimaryButton>
                        </View>
                        <View style={styles.buttonsWrapper}>
                            <PrimaryButton onPress={ResetHandler}>
                                Reset
                            </PrimaryButton>
                        </View>
                    </View>
                    <View style={styles.supportButtons}>
                        <View style={styles.buttonsWrapper}>
                            <PrimaryButton onPress={props.giveUp} buttonStyle={styles.giveUpButton}>
                                Give up
                            </PrimaryButton>
                        </View>
                        <View style={styles.buttonsWrapper}>
                            <PrimaryButton onPress={props.giveUp} buttonStyle={styles.hintButton}>
                                Hint
                            </PrimaryButton>
                        </View>
                    </View>
                </Card>
                {/*<Card>*/}
                {/*    <FlatList data={guessedNumbers}*/}
                {/*              renderItem={data =>*/}
                {/*                  <GuessedNumbers/>*/}

                {/*              }*/}
                {/*    />*/}
                {/*</Card>*/}
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    titleText: {
        fontSize: 20,
        marginVertical: 10
    },
    inputContainer: {
        width: '80%',
        minWidth: 200,
        maxWidth: '95%',
        alignItems: 'center',
    },
    inputWrapper: {
        marginVertical: 10,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        width: '50%',
        textAlign: 'center',
    },
    buttonsWrapper: {
        width: "40%"
    },
    supportButtons: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        marginVertical: 10
    },
    giveUpButton: {
        backgroundColor: 'red'
    },
    hintButton: {
        backgroundColor: 'magenta'
    }
});

export default GameScreen;
