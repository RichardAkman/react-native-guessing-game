import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from "../constants/colors";

const Header = props => {

    return (
        <View style={styles.headerWrapper}>
            <Text style={styles.headerTitle}>
                {props.title}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    headerWrapper: {
        width: '100%',
        height: 90,
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerTitle: {
        fontSize: 18,
        color: 'white'
    }
});

export default Header;
