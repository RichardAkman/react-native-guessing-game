import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

const Input = props => {

    return (
        <TextInput {...props} style={{...styles.input, ...props.style}}/>
    );
};

const styles = StyleSheet.create({
    input: {
        borderColor: '#707070',
        borderWidth: 1,
        borderRadius: 12,
        backgroundColor: '#FFFFFF',
        padding: 5
    },
});

export default Input;
