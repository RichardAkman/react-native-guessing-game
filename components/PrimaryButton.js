import React from 'react';
import {StyleSheet, TouchableOpacity, View, Text, Dimensions} from 'react-native';
import Colors from "../constants/colors";

const PrimaryButton = props => {

    return (
        <TouchableOpacity onPress={props.onPress}>
            <View style={{...styles.button, ...props.buttonStyle}}>
                <Text style={styles.buttonText}>{props.children}</Text>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: Colors.primary,
        paddingHorizontal: 10,
        paddingVertical: 5,
        alignItems: 'center',
        minWidth: Dimensions.get('window').width / 4
    },
    buttonText: {
        color: 'white',
        fontFamily: 'open-sans'
    }
});

export default PrimaryButton;
