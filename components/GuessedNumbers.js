import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

const GuessedNumbers = props => {

    return (
        <View style={styles.container}>
            <Text>{props.children}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        borderWidth: 2,
        padding: 10,
        borderRadius: 10
    },
});

export default GuessedNumbers;
