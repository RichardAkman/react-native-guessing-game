import React from 'react';
import {StyleSheet, View} from 'react-native';

const Card = props => {

    return (
        <View style={{...styles.mainContainer, ...props.style}}>
            {props.children}
        </View>
    );
};

const styles = StyleSheet.create({
    mainContainer: {
        justifyContent: 'center',
        shadowColor: 'black',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowRadius: 5,
        shadowOpacity: 0.7,
        backgroundColor: 'white',
        elevation: 5,
        padding: 20,
        borderRadius: 20,
    },
});

export default Card;
