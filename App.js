import React, {useState} from 'react';
import {StyleSheet, View, Dimensions} from 'react-native';
import Header from "./components/Header";
import GameScreen from "./screens/GameScreen";
import HomeScreen from "./screens/HomeScreen";
import GameOverScreen from "./screens/GameOver";
import * as Font from 'expo-font';
import {AppLoading} from 'expo';

const getFonts = () => {
    Font.loadAsync({
        'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
        'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
    })
}

// console.log(Dimensions.get('window'))

export default function App() {
    const [gameStarted, setGameStarted] = useState(false)
    const [gameOver, setGameOver] = useState(false)
    const [gameWon, setGameWon] = useState(false)
    const [stuffLoaded, setStuffLoaded] = useState(false)
    let currentScreen;
    let header;

    const toggleGameStartedStatus = () => {
        setGameStarted(!gameStarted);
    }

    const gameOverHandler = gameWon => {
        setGameOver(true);
        setGameStarted(false);
        setGameWon(gameWon);
    }

    const giveUpHandler = () => {
        setGameOver(false);
        setGameStarted(false);
        setGameWon(false);
    }

    const resetStateHandler = () => {
        setGameOver(false);
        setGameStarted(false);
    }

    if (!stuffLoaded) {
        currentScreen = <AppLoading
            startAsync={getFonts}
            onFinish={() => setStuffLoaded(true)}
            onError={(err) => console.log(err)}
        />
    } else {
        currentScreen = <HomeScreen startGame={toggleGameStartedStatus}/>
        header = <Header title="Lukit"/>
    }

    if (gameStarted) {

        let numberToGuess = Math.floor(Math.random() * 100);
        console.log(numberToGuess)
        currentScreen = <GameScreen numberToGuess={numberToGuess} endGame={gameOverHandler} giveUp={giveUpHandler}/>
    }

    if (gameOver) {
        let msg;

        if (gameWon) {
            msg = 'Yay, you win. Good job!'
        } else {
            msg = 'Oh no, you lost :('
        }

        currentScreen = <GameOverScreen message={msg} goHome={resetStateHandler}/>
    }

    return (
        <View style={styles.screen}>
            {header}
            {currentScreen}
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    }
});
